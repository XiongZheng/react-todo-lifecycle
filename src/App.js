import React, {Component} from 'react';
import './App.less';
import TodoList from "./components/TodoList";
import Button from "./components/Button";

const refreshPage = () => location.reload()

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      show: true,
    }
  }

  handleShow = () => {
    this.setState({show: true})
  }

  handleHide = () => {
    this.setState({show: false, list: []})
  }

  handleRefresh = () => {
    refreshPage()
  }

  handleListChange = (list) => {
    this.setState({list})
  }

  render() {
    const {
      state: {show, list},
      handleShow,
      handleHide,
      handleRefresh,
      handleListChange
    } = this
    return (
      <div className='App'>
        <div style={{marginBottom: '20px'}}>
          {
            show ? <Button onClick={handleHide}>Hide</Button> : <Button onClick={handleShow}>Show</Button>
          }
          <Button onClick={handleRefresh} style={{float: 'right'}}>Refresh</Button>
        </div>
        {
          show ? <TodoList list={list} onChange={handleListChange}/> : null
        }
      </div>
    )
  }
}
;

export default App;